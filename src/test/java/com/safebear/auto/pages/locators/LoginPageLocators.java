package com.safebear.auto.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;

@Data
public class LoginPageLocators {

    //fields
    private By usernameLocator =By.xpath(".//input[@id='username']");
    private By passwordLOcator=By.xpath(".//input[@id='password']");

    //buttons
    private By loginLocator = By.xpath(".//input[@id='password']");

    //message
    private By failedLoginMessage=By.xpath(".//b[.=WARNING: Username or Password is incorrect]");

}
