package com.safebear.auto.tests;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(
        //This is where my html report wil be (in the target/cucumber directory)
        plugin = {"pretty","html:target/cucumber"},
        //Tis is changing to 'not @to-do' in future cucmber releases
        tags="~@to-do",
        glue = "com.safebear.auto.tests",
        features = "classpath:toolslist.features/login.feature"
)

        public class RunCukes extends AbstractTestNGCucumberTests {


}

